module Invoice exposing (InvoiceTextData, invoiceLayout, a4paper, ColumnSize, CurrencyPosition(..))

import Html exposing (..)
import Html.Attributes exposing (..)


type alias InvoiceTextData =
  { leftHeaderRegular : String
  , leftHeaderItalic : String
  , rightHeader : String
  , rightSubHeader : String
  , leftEntries : List ( List ( String, String ) )
  , rightEntries : List ( List ( String, String ) )
  , currency : String
  , currencyPosition : CurrencyPosition
  , sumTotal : String
  , dueDate : String
  , tableHeaders : List String
  , tableEntries : List (List String)
  , trailingValues : List (String, String)
  }

invoiceLayout : InvoiceTextData -> Html msg
invoiceLayout data =
  div
    [ style "font-family" "\"Segoe UI\", \"Helvetica Neue\", Arial, sans-serif"
    , style "height" "100%"
    , style "display" "grid"
    ]
    [ columns
      [ ( leftHeader data.leftHeaderRegular data.leftHeaderItalic, Fr 1 )
      , ( rightHeaderLayout
          ( invoiceNrHeader data.rightHeader )
          ( dateHeader data.rightSubHeader )
        , Fr 1
        )
      ]
    , columns
      [ ( keyValueLayout data.leftEntries, Fr 1 )
      , ( keyValueLayout data.rightEntries, Fr 1 )
      ]
    , columns
      [ ( sumLayout data.currency data.currencyPosition data.sumTotal data.dueDate, Fr 1 )
      , ( div [] [], Fr 1 )
      ]
    , div []
      [ tableLayout data.tableHeaders data.tableEntries
      , br [] []
      , trailingValuesLayout data.trailingValues
      ]
    ]

a4paper : Html msg -> Html msg
a4paper innerElement =
  div
    [ style "display" "grid"
    , style "grid-template-columns" "auto 210mm auto"
    , style "grid-template-rows" "297mm"
    ]
    [ div [] []
    , div
      [ style "overflow" "hidden"
      , style "padding" "6.35mm"
      ]
      [ innerElement ]
    , div [] []
    ]

type ColumnSize
  = Fr Float

columnSizeToString : ColumnSize -> String
columnSizeToString gs =
  case gs of
    Fr val -> String.fromFloat val ++ "fr"


columns : List (Html msg, ColumnSize) -> Html msg
columns cs =
  let
    columnSizes : String
    columnSizes =
      List.map ( columnSizeToString << Tuple.second ) cs
      |> List.intersperse " "
      |> String.concat

    inDiv : Html msg -> Html msg
    inDiv element = div [] [element]
  in
    div
      [ style "display" "grid"
      , style "grid-template-columns" columnSizes
      ]
      ( List.map ( inDiv << Tuple.first ) cs )

-- # Headers
-- ## Left header
leftHeader : String -> String -> Html a
leftHeader part1 part2 =
  h1
    [ style "font-weight" "400"
    , style "font-size" "2.5rem"
    , style "margin" "0"
    , style "padding" "0"
    ]
    [ text part1
    , i
      [ style "font-weight" "100" ]
      [ text part2 ]
    ]

-- ## Right header
invoiceNrHeader : String -> Html a
invoiceNrHeader str =
  h1
    [ style "font-weight" "400"
    , style "font-size" "2.5rem"
    , style "letter-spacing" "7px"
    , style "margin" "0"
    , style "padding" "0"
    ]
    [ text str ]


dateHeader : String -> Html a
dateHeader str =
  h5
    [ style "color" "DarkGrey"
    , style "font-weight" "400"
    , style "font-size" "1.25rem"
    , style "margin" "0"
    , style "padding" "0"
    ]
    [ text str ]

rightHeaderLayout : Html a -> Html a -> Html a
rightHeaderLayout topHtml bottomHtml =
  div
    [ style "text-align" "center"
    , style "display" "grid"
    , style "grid-template-columns" "1fr"
    , style "grid-template-rows" "45px 1fr"
    ]
    [ topHtml
    , bottomHtml
    ]

-- # Key value list

primary_color = "rgb(56, 115, 178)"

keyValueLayout : List (List (String, String)) -> Html msg
keyValueLayout entries =
  let
    entryLayout : List (String, String) -> Html msg
    entryLayout entry =
      case entry of
        [] -> div [] []
        (headerKey, headerValue) :: entryRemainder ->
          entryHeaderLayout headerKey headerValue
            :: List.map entryBodyLayout entryRemainder
            |> List.map (\(a, b) -> [a, b])
            |> List.concat
            |> div
              [ style "display" "grid"
              , style "grid-template-columns" "2fr 5fr"
              , style "grid-column-gap" "10px"
              ]

    entryHeaderLayout : String -> String -> (Html msg, Html msg)
    entryHeaderLayout k v =
      ( div
        [ style "text-align" "right"
        , style "color" primary_color
        ]
        [ text k ]
      , div [] [ text v ]
      )
    entryBodyLayout : ( String, String ) -> (Html msg, Html msg)
    entryBodyLayout (k, v) =
      ( div [ style "text-align" "right" ] [ text k ], div [] [ text v ])
  in
    div
      [ style "display" "grid"
      , style "grid-template-columns" "1fr"
      , style "grid-row-gap" "15px"
      ]
      ( List.map entryLayout entries )

-- sum total
type CurrencyPosition
  = BeforeNumber
  | AfterNumber

sumLayout : String -> CurrencyPosition -> String -> String -> Html msg
sumLayout currency currencyPosition sumTotal subText =
  let
    currencyElement : Html msg
    currencyElement =
      span
        []
        [ text currency ]
    sumTotalElement : Html msg
    sumTotalElement =
      span
        [ style "font-weight" "400"
        , style "font-size" "25pt"
        , style "line-height" "25pt"
        ]
        [ text sumTotal ]
    sumContainerElement : List (Html msg) -> Html msg
    sumContainerElement elements =
      div
        [ style "display" "grid"
        , style "grid-template-columns" "max-content max-content auto"
        , style "grid-column-gap" "3px"
        , case currencyPosition of
            BeforeNumber -> style "align-items" "start"
            AfterNumber -> style "align-items" "end"
        ]
        elements
  in
  div
    []
    [ case currencyPosition of
        BeforeNumber -> sumContainerElement [ currencyElement, sumTotalElement, div [] [] ]
        AfterNumber -> sumContainerElement [ sumTotalElement, currencyElement, div [] [] ]
    , div
      [ style "color" "DarkGrey" ]
      [ text subText ]
    ]


-- table

type TableEntry
  = Text String
  | Monetary Int Int

invoice_th : String -> Html msg
invoice_th input =
  th
    [ style "color" primary_color
    , style "border-top" "1px solid black"
    , style "border-bottom" "1px solid black"
    , style "font-weight" "400"
    , style "line-height" "200%"
    , style "vertical-align" "middle"
    , style "text-align" "center"
    ]
    [ text input ]

invoice_td : String -> Html msg
invoice_td input =
  td
    [ style "line-height" "200%"
    , style "text-align" "center"
    ]
    [ text input ]

invoice_td_tail : Html msg
invoice_td_tail =
  td [ style "border-bottom" "1px solid black" ] []

tableLayout : List String -> List (List String) -> Html msg
tableLayout headers entries =
  div []
  [ table
    [ style "width" "100%"
    , style "border-collapse" "collapse"
    ]
    ( tr [] ( List.map (\t -> invoice_th t) headers )
    :: ( List.map ( tr [] << List.map (\t -> invoice_td t) ) entries )
    ++ [ tr [] ( List.map (\t -> invoice_td_tail) headers ) ]
    )
  ]

trailingValuesLayout : List (String, String) -> Html msg
trailingValuesLayout entries =
  div
    [ style "display" "grid"
    , style "grid-template-columns" "auto max-content max-content"
    , style "grid-template-rows" "max-content"
    , style "grid-column-gap" "5pt"
    ]
    <| List.concat <| List.map
      ( \(k, v) ->
        [ span [] []
        , span
          [ style "text-align" "right"
          , style "color" primary_color
          ]
          [ text k ]
        , span [ style "text-align" "right" ] [ text v ]
        ]
      ) entries
